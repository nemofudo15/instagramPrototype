@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-4 " >
            <img src="/storage/{{$user->profile->insertimage()}}"   class="rounded-circle w-100" alt="">
        </div>
        <div class="col-8 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="h4"> {{$user -> username}} </div>
                <div class="d-flex align-items-center pb-3">
                    <follow-button user-id="{{$user->id}}" follows="{{$follows}}"></follow-button>
                </div>
                @can ('update', $user->profile)
                    <a href="/p/create">New post</a>
                @endcan
                
            </div>
            @can ('update', $user->profile)
                <a href="/profile/{{$user->id}}/edit">Edit profile</a>
            @endcan
            <div class="d-flex ">
                <div class="row pl-3 pr-5"><strong class="pr-2">{{$postCount}}</strong>posts</div>
                <div class="row pl-3 pr-5"><strong class="pr-2">{{$followersCount}}</strong>followers</div>
                <div class="row  pl-3 pr-5"><strong class="pr-2">{{$followingCount}} </strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{$user->profile->description}}</div>
                <div><a href="#">{{$user->profile->url}}</a>
            </div>

            
        </div>
    </div>
    <div class="row pt-5">
        @foreach ($user->posts as $post)
            <div class="col-4 pb-4">
                <a href="/p/{{$post->id}}">
                    <img src="/storage/{{$post->image}}" class="w-100 ">   
                </a>
                 
            </div>            
        @endforeach

    </div>
    
</div>
@endsection
