<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/email',[App\Mail\NewUserWelcomeMail::class,'build']);
Route::get  ('/', [App\Http\Controllers\PostsController::class,'index']);
Route::get('/p/create', [App\Http\Controllers\PostsController::class, 'create']);
Route::get  ('/p/{post}', [App\Http\Controllers\PostsController::class,'show']);
Route::post('/p', [App\Http\Controllers\PostsController::class,'store']);
Route::get  ('/profile/{user}/edit', [App\Http\Controllers\ProfilesController::class,'edit'])->name('profile.edit');//Este simplemente nos mostrara el formulario con los datos 
Route::patch  ('/profile/{user}', [App\Http\Controllers\ProfilesController::class,'update'])->name('profile.update');//Este actualiza la información
Route::get('/profile/{user}', [App\Http\Controllers\ProfilesController::class, 'index'])->name('profile.show');
Route::post('follow/{user}', [App\Http\Controllers\FollowsController::class, 'store']);

