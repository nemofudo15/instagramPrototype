<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Intervention\Image\Facades\Image;       
use Illuminate\Support\Facades\Cache;
class ProfilesController extends Controller
{
    public function index($user)
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user) : false;
        $u = User::findOrFail($user);
        $postCount = Cache::remember(
            'count.posts.'. $user,now()->addSeconds(30) ,
            function() use($u){
                return $u->posts->count();
        });
        $followersCount = Cache::remember(
            'count.followers.'. $user,now()->addSeconds(30) ,
            function() use($u){
                return $u->profile->followers->count();
        });
        $followingCount = Cache::remember(
            'count.following.'. $user,now()->addSeconds(30) ,
            function() use($u){
                return $u->profile->followers->count();
        });
        return view('profiles.index',[
            'user' => $u,
            'follows'=>$follows,
            'postCount'=> $postCount,
            'followersCount' => $followersCount,
            'followingCount' => $followingCount
        ]);
    }

    public function edit(\App\Models\User $user){ //Es lo mismo que usar  en ves  el findorFail en user
        $this->authorize('update', $user->profile);
        return view('profiles.edit',compact('user')); //compact es similar a poner como argumento el arreglo [ 'user'=> $user]
    }

    public function update(User $user){
        
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'url' => 'url',
            'image' => 'image',

        ]);

        if(request('image')){
            $imagepath = request('image')->store('profile','public');
            $image = Image::make(public_path("/storage/{$imagepath}"))->fit(1000,1000);
            $image->save();
            $imageArray = ['image'=> $imagepath];
        }
        auth()->user()->profile->update(array_merge(
            $data,
            $imageArray ?? []//This is useful if it has to be overriden the array of $data with distint value of 'image' 
        ));
        return redirect("/profile/{$user->id}");
    }
}

