<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $guarded  = []; //Disabilate security for mass assigment 
    public  function user(){
        return $this->belongsTo(User::class);
    }


    public function insertimage(){
        $imagePath = ($this->image) ? $this->image : '/profile/YKdrDeHe0o9gObxEJNZQTGs0qjG6LnsbUtRCNXe0.png';
        return  $imagePath;
    }

    public function followers(){
        return $this->belongsToMany(User::class);
    }
}
